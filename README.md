# ts-ecs-game

[![license](https://img.shields.io/badge/license-MIT%2FApache--2.0-blue")](LICENSE-MIT)
[![docs](https://img.shields.io/badge/docs-typescript-blue.svg)](https://aicacia.gitlab.io/libs/ts-ecs-game/)
[![npm (scoped)](https://img.shields.io/npm/v/@aicacia/ecs-game)](https://www.npmjs.com/package/@aicacia/ecs-game)
[![pipelines](https://gitlab.com/aicacia/libs/ts-ecs-game/badges/master/pipeline.svg)](https://gitlab.com/aicacia/libs/ts-ecs-game/-/pipelines)

ecs plugins and components for 2d/3d interactions
