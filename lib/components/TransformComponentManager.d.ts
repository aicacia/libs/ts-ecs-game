import { DefaultDescriptorManager } from "@aicacia/ecs/lib/DefaultDescriptorManager";
import type { TransformComponent } from "./TransformComponent";
export declare class TransformComponentManager extends DefaultDescriptorManager<TransformComponent> {
}
